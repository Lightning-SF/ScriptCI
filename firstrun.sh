#! /bin/bash

# ScriptCI
# FirstRun Script




######################################
# MUST ONLY BE EXECUTED ON FIRSTRUN!##
# THERE ARE SECURITY CHECKS IN PLACE!#
######################################

h=`pwd`


echo ScriptCI First run initiated

echo Now reading tool.txt

jtool=`cat tool.txt`

if [ $jtool = "mvn" ];
  then
    echo Your tool is Maven! Now downloading Maven!
    cd $h/scripts
    sh getmvn.sh
    cd $h
fi

if [ $jtool = "gradle" ];
  then
    echo Your tool is Gradle! Now downloading Gradle!
    cd $h/scripts
    sh getgradle.sh
    cd $h
fi

if [ $jtool = "ant" ];
  then
    echo Your tool is Ant! Now downloading Ant!
    cd $h/scripts
    sh getant.sh
    cd $h
fi

echo Finished fetching your tool! Running post-download actions

if [ $? = "0" ];
  then
    echo Exited with 0, writing to file
    cd $h
    echo 0 > doNotTouch.txt
fi

echo ScriptCI Finished First Run!
echo Please do not run again
