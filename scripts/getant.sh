#! /bin/bash
h=`pwd`

antversion=1.9.4
ant=http://mirror.nus.edu.sg/apache/ant/binaries

cd $h/../tools/java/ant
curl -O $ant/apache-ant-$antversion-bin.tar.gz
tar -zxf apache-ant-$antversion-bin.tar.gz
