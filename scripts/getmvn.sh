#! /bin/bash
h=`pwd`
version=3.3.3
mvnFirstDigit=3
mirror=http://mirror.nus.edu.sg/apache/maven/maven-$mvnFirstDigit/$version/binaries

cd $h/../tools/java/maven
curl -O $mirror/apache-maven-$version-bin.tar.gz
tar -zxf apache-maven-$version-bin.tar.gz
