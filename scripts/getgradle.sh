#! /bin/bash
h=`pwd`

gversion=2.4
gradle=https://services.gradle.org/distributions

cd $h/../tools/java/gradle
curl -O $gradle/gradle-$gversion-bin.zip
unzip gradle-$gversion-bin.zip
