# ScriptCI

## What is it?
ScriptCI is a **research** Continuous Integration tool. You **cannot** deploy it to web.
This is **purely** for research, but it may also come in handy for you if you happen to be
running low on system resources.

### Redeeming features
1. ScriptCI **hardly** uses any system resources (Just the amount of memory the build tool
needs + some memory [less than 50MiB]
